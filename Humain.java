public class Humain {
  private String name;
  private String ethnicity;
  private int age;
  private double height;
  private double weight;
// Set Methods
  public void growOlder() {
    System.out.println("You have grown older. You are now "+(this.age + 1)+" year old!");
  }
/*  public void setName(String n) {
    this.name = n;
  } */
  public void setEthnicity(String e) {
    this.ethnicity = e;
  }
  public void setAge(int a) {
    this.age = a;
  }
  public void setHeight(double h) {
    this.height = h;
  }
  public void setWeight(double w) {
    this.weight = w;
  }
  // Get Methods
  public String getName() {
    return this.name;
  }
  public String getEthnicity() {
    return this.ethnicity;
  }
  public int getAge() {
    return this.age;
  }
  public double getHeight() {
    return this.height;
  }
  public double getWeight() {
    return this.weight;
  }
  // Constructor 
  public Humain (String n, String e, int a, double h, double w) {
  this.name = n;
  this.ethnicity = e;
  this.age = a;
  this.height = h;
  this.weight = w;
  }
}